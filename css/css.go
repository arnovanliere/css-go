package css

import (
    "fmt"
    "io/ioutil"
    "os"
    "strconv"
    "strings"
)

type Variable struct {
    Name         string
    Value        string
    Replacements int
}

type CSS struct {
    InputFile        string
    VariableFile     string
    OutputFile       string
    FileContents     string
    FileContentsVars string
    Variables        []Variable
}

/**
 * Find all occurrences of 'substr' in 'string'
 *   string The string to search in
 *   substr The string to search for
 */
func findAll(string string, substr string) []int {
    var indices []int
    for true {
        index := strings.Index(string, substr)
        if index != -1 && index != 0 {
            indices = append(indices, index)
            string = string[index:]
        } else {
            break
        }
    }
    return indices
}

/**
 * Replace 'name' with 'value' at position 'pos' in 'string'
 *   string The string to search in
 *   pos    The start position of 'name'
 *   name   The value to replace
 *   value  The value to replace it with
 */
func replace(string* string, pos int, name string, value string) {
    first := (*string)[:pos] // Part before 'name'
    last := (*string)[pos + len(name):] // Part after 'name'
    *string = first + value + last
}

/**
 * Determine max-width of 'name' if index == 0 or 'values' if index == 1
 *   css   Instance of CSS to get max-width of
 *   index 0 for width of name and 1 or width of values
 */
func maxWidth(css CSS, index int) int {
    max := 4 // For 'name'
    if index == 1 {
        max = 5 // For 'values'
    }

    for _, v := range css.Variables {
        if index == 0 && len(v.Name) > max {
            max = len(v.Name)
        } else if index == 1 && len(v.Value) > max {
            max = len(v.Value)
        }
    }

    return max + 2 // Padding of 2 characters
}

/*
 * Parse the variables. Save them in css.Variables.
 */
func (css* CSS) ParseVariables() {
    prev, next, name, value := " ", "", "", ""
    cur := string(css.FileContentsVars[0])
    size, index := len(css.FileContentsVars) - 1, 0

    for index < size {
        next = string(css.FileContentsVars[index + 1])
        // Variable-names start with '--' and end with a ':'
        // 'prev' should be a space, tab or linebreak to prevent
        // variable use to be recognised as variable declaration
        if cur == "-" && next == "-" &&
            (prev == " " || prev == "\n" || prev == "\t") {
            // Reset 'name' and 'value'
            name, value = "", ""
            for cur != ":" && index != size {
                name += cur
                index++
                cur = string(css.FileContentsVars[index])
            }
            // Skip all spaces and the ':' after 'name'
            for (cur == " " || cur == ":") && index != size {
                index++
                cur = string(css.FileContentsVars[index])
            }
            // Only after a variable-name follows a variable-value
            // Variable-value ends with ';'
            for cur != ";" && index != size {
                value += cur
                index++
                cur = string(css.FileContentsVars[index])
            }
            css.Variables = append(css.Variables, Variable{
                Name: name,
                Value: value,
                Replacements: 0,
            })
        }
        index++
        prev = cur
        cur = string(css.FileContentsVars[index])
    }
}

/*
 * Replace the variables in css.FileContents with css.Variables.
 */
func (css* CSS) ReplaceVariables() {
    output := css.FileContents
    for pos, variable := range css.Variables {
        replacements := 0
        name := "var("+variable.Name+")"
        value := variable.Value
        positions := findAll(output, name)
        for _, pos := range positions {
            replacements++
            replace(&output, pos, name, value)
        }
        css.Variables[pos].Replacements = replacements
    }
    err := os.Remove(css.OutputFile)
    err = ioutil.WriteFile(css.OutputFile, []byte(output), 0777)
    if err != nil {
        fmt.Printf("ERROR: \"%s\" could not be written\n", css.OutputFile)
        os.Exit(1)
    }
    fmt.Printf("\"%s\" successfully saved\n", css.OutputFile)
}

/*
 * Write the stats for the replacements.
 */
func (css CSS) WriteStats() {
    nameWidth := strconv.Itoa(maxWidth(css, 0))
    valWidth := strconv.Itoa(maxWidth(css, 1))
    fmt.Printf("%-"+nameWidth+"s %-"+valWidth+"s %-13s\n", "Name", "Value", "Replacements")
    for _, v := range css.Variables {
        fmt.Printf("%-"+nameWidth+"s %-"+valWidth+"s %-13d\n", v.Name, v.Value, v.Replacements)
    }
}
