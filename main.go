package main

import (
    "css/css"
    "fmt"
    "io/ioutil"
    "os"
)

/*
 * Assign the contents of 'file' to 'contents'
 *   file     The name of the file to read
 *   contents The variable to assign contents to
 */
func assignContents(file string, contents *string) {
    f, err := ioutil.ReadFile(file)
    if err != nil {
        fmt.Printf("Error reading \"%s\"\n", file)
        os.Exit(1)
    }
    *contents = string(f)
}

func main() {
    args := os.Args[1:]
    if len(args) < 3 {
        fmt.Println("Invalid number of arguments. Usage: css <input file> <variables file> <output file>")
        os.Exit(1)
    }

    c := css.CSS{
        InputFile:        args[0],
        VariableFile:     args[1],
        OutputFile:       args[2],
        FileContents:     "",
        FileContentsVars: "",
    }

    assignContents(c.InputFile, &c.FileContents)
    assignContents(c.VariableFile, &c.FileContentsVars)

    c.ParseVariables()
    c.ReplaceVariables()
    c.WriteStats()
}
